import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestConverter {

    @Test
    public void testConversion() {
        Converter converter = new Converter();
        double value = converter.convert(30.0);
        Assertions.assertEquals(86.0, value);
    }
}
